﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbarSample
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SecondPage : ContentPage
	{
        private string[] data= new string[]
        {
            "1","2","3","4","5","6","7","8","9","10","11","12","13","14",
        };
		public SecondPage ()
		{
			InitializeComponent ();
		    BindingContext = data;
		}

	    async void OnSelection(object sender, SelectedItemChangedEventArgs e)
	    {
	        await Navigation.PushAsync(new SecondDetailPage(), true);
	    }
    }
}